﻿var EDPDistribuicaoOutOfLight = angular.module("EDPDistribuicao.OutOfLight", [
    'ui.router',
     'ngSanitize',
     'EDPD.Services'
])
.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

}]);
EDPDistribuicaoOutOfLight.controller('OutOfLightImageCtrl', ['$scope', '$rootScope', '$stateParams', '$state', '$filter', '$window', '$location', '$timeout', '$sce', 'NativeAppComunicationService',
function ($scope, $rootScope, $stateParams, $state, $filter, $window, $location, $timeout, $sce, NativeAppComunicationService) {
    $scope.isFlashAvailable = false;
    $scope.flashState = $rootScope.flashState? $rootScope.flashState: false;
    $scope.rs = "nao";
    $scope.img_src = $scope.flashState ? "../assets/img/menu/bt_lanternaON.png" : "../assets/img/menu/bt_lanterna.png";
    NativeAppComunicationService.on('FLASHLIGHT', function (result) {
        var resp = angular.fromJson(result);
        $scope.rs = result;
        $scope.isFlashAvailable = resp.AVAILABLE == '1' ? true : false;
        $scope.flashState = resp.LIGHT == '0' ? false : true;

        $scope.$apply(function () {
            $rootScope.flashState =$scope.flashState;
            $scope.img_src = $scope.isFlashAvailable && $scope.flashState ? "../assets/img/menu/bt_lanternaON.png" : "../assets/img/menu/bt_lanterna.png";
        });

    });
   
    $scope.InitFlashLight = function () {
        try {
            
            NativeAppComunicationService.PostMessage({ 'FLASHLIGHT': 'ON' });
        } catch (e) { }
    };
    $scope.on = function () {
        NativeAppComunicationService.PostMessage({ 'FLASHLIGHT': 'ON' });
    };
    $scope.off = function () {
        NativeAppComunicationService.PostMessage({ 'FLASHLIGHT': 'OFF' });
    };
    $scope.toggle = function () {
        NativeAppComunicationService.PostMessage({ 'FLASHLIGHT': $scope.flashState ? 'OFF' : 'ON' });
    };


    
    $scope.$on('$locationChangeStart', function (event, next, current) {
        // init out of light page
        if (current.match("\/menu") && next.match("\/page.outoflight")) {
            $rootScope.flashState = false;
            $scope.InitFlashLight();
        }

        if (next.match("\/menu") && current.match("\/page.outoflight")) {
            $scope.off();
        }
    });
    $scope.GetImageSrc = function () {
        return $scope.flashState ? "../assets/img/menu/bt_lanternaON.png" : "../assets/img/menu/bt_lanterna.png";
    };
}]);
EDPDistribuicaoOutOfLight.controller('OutOfLightCtrl', ['$scope', '$stateParams', '$state', '$filter', '$timeout', '$window', '$location', '$anchorScroll', '$sce', '$log', 'OutOfLightService', 'NativeAppComunicationService', 'modalService',
function ($scope, $stateParams, $state, $filter, $timeout, $window, $location, $anchorScroll, $sce, $log, OutOfLightService, NativeAppComunicationService, modalService) {
    $scope.WizzardMatrix = $stateParams.WizzardMatrix;
    $scope.PrevSteps = $stateParams.prev || [];
    $scope.Data = $stateParams.data || [];
    $scope.Data['CPE_LIST'] = $scope.Data['CPE_LIST'] || [{ cpe: null, description: 'Sem local de consumo' }];
    $scope.Data['SELECTED_CPE'] = $scope.Data['SELECTED_CPE'] || { cpe: null };
    $scope.PrevStep;
    $scope.CurrentStep = $stateParams.current;
    $scope.CPEsInfo = $scope.Data['CPE_LIST'];
    $scope.SelectedCPE = $scope.Data['SELECTED_CPE'];
    /*Get css units vw/vh to pixels*/
    $scope.width = $(window).width();
    $scope.height = $(window).height();
    $scope.getWidth = function (width) {
        return (width / 100) * $scope.width + 'px';
    }
    $scope.getHeight = function (height) {
        return (height / 100) * $scope.height + 'px';
    };

    $scope.PopulateMatrix = function () {

        if ($scope.WizzardMatrix === null) {

            OutOfLightService.GetWizzard({}).$promise.then(
                function (data) {
                    if (data.Success && data.Result && data.Result.Matrix) {
                        $scope.WizzardMatrix = data.Result.Matrix.questions;

                        $scope.SetupAppCom();

                        if ($scope.CurrentStep === null && $state.current.name === 'page.outoflight.start') {
                            $scope.CurrentStep = $scope.WizzardMatrix[0];
                        }
                    }
                },
                function (err) { $log.error(err); }
            );
        }
    };
    $scope.ExecuteAction = function (action) {

        if (action.gotoquestion || action.gotodatastep) {
            angular.forEach($scope.WizzardMatrix, function (qValue, qKey) {

                if (qValue.id === action.gotoquestion || qValue.id === $scope.Data[action.gotodatastep]) {
                    $scope.PrevStep = $scope.CurrentStep;

                    angular.forEach($scope.PrevStep.answers, function (aValue, aKey) {
                        if (aValue.gotoquestion === action.gotoquestion) {
                            $scope.PrevStep.answers[aKey].selected = true;
                        }
                    });
                    $scope.PrevSteps.push($scope.PrevStep);

                    $scope.CurrentStep = $scope.WizzardMatrix[qKey];

                    if ($scope.PrevStep.questionOrdinal) {
                        $scope.CurrentStep.questionOrdinal = $scope.PrevStep.questionOrdinal + 1;
                    } else {
                        $scope.CurrentStep.questionOrdinal = 1;
                    }

                    if ($scope.CurrentStep.isStart) {
                        $state.go('^.start', { WizzardMatrix: $scope.WizzardMatrix, prev: $scope.PrevSteps, current: $scope.CurrentStep });
                    } else {
                        if ($scope.CurrentStep.isEnd) {
                            $scope.Data["CPE"] = $scope.SelectedCPE;
                            $state.go('page.outoflight.end', { WizzardMatrix: $scope.WizzardMatrix, prev: $scope.PrevSteps, current: $scope.CurrentStep, data: $scope.Data });
                        }
                        else {
                            $scope.Data["endstep"] = action.endstep;
                            $state.go('^.step', { WizzardMatrix: $scope.WizzardMatrix, prev: $scope.PrevSteps, current: $scope.CurrentStep, data: $scope.Data });
                        }
                    }
                }

            });
        }
        else {
            if (action.gotoaction) {
                $scope.ShowModal(action);
            }
            else {
                if (action.gotoexternalaction) {
                    switch (action.gotoexternalaction) {
                        case 'CALL_AGENT':
                            try {
                                NativeAppComunicationService.invokeCALLPHONE('800916916');
                            } catch (e) { $log.warn(e.message); }
                            break;
                        case 'MENU':
                            try {
                                // NativeAppComunicationService.invokeMENU();
                                $state.go("menu");
                            } catch (e) { $log.warn(e.message); }
                            break;
                        default:

                    }
                }
            }
        }
    };
    $scope.ScrollToCurrent = function () {
        var old = $location.hash();
        $location.hash('currentStep');
        $anchorScroll();
        //reset to old to keep any additional routing logic from kicking in
        $location.hash(old);
    };
    $scope.ShowModal = function (action) {
        
         modalService.showModal(
            {
                templateUrl: 'help_' + action.gotoaction + '.html'
            },
            {
                closeButtonText: 'Fechar',
                actionButtonText: null,
                headerText: action.gotoaction,
            }
        );
        
    };
    $scope.setCPEsInfo = function (info) {
        $scope.$apply(function () {
            $scope.Data['CPE_LIST'] = [{ cpe: null, description: 'Sem local de consumo' }].concat(info);
            if (info.length > 0) {
                $scope.SelectedCPE.cpe = $scope.Data['SELECTED_CPE'].cpe = '';
            }
            $scope.CPEsInfo = $scope.Data['CPE_LIST'];
        });
    };
    $scope.setSelectedCPE = function (cpe) {
        $scope.SelectedCPE.cpe = cpe;
    }
    $scope.callNative = function (prop) {
        //NativeAppComunicationService.invokeDUMMY(prop)
    };
    $scope.SetupAppCom = function () {

        NativeAppComunicationService.PostMessage({ 'SQL': 'SELECT' });
    };
    NativeAppComunicationService.on('SQL', function (result) {
        try {
            var resp = angular.fromJson(result);
            var oper = resp.OPERATION;
            switch (oper) {
                case 'SELECT':
                    $scope.setCPEsInfo(resp.RESULT);
                    break;
                case 'DELETE':
                    $scope.cpe_list = $filter('filter')($scope.cpe_list, { id: !$scope.toDeleteId });
                    $scope.toDeleteId = null;
                    break;
                default:

                    break;
            }
        } catch (e) {
        }
        

    });
    $scope.PopulateMatrix();

    $timeout(function () {
        $scope.ScrollToCurrent();
    });
    $scope.LoadCPE = function () {
        var cpe_list = [];
        var query = "select id, cpe, desc, ativo from instalation;";
        $cordovaSQLite.execute(db, query, []).then(
            function (res) {
                try {

                    for (var i = 0; i < res.rows.length; i++) {
                        cpe_list.push({ id: res.rows.item(i).id, cpe: res.rows.item(i).cpe, desc: res.rows.item(i).desc, ativo: res.rows.item(i).ativo });
                    }
                    $scope.setCPEsInfo(cpe_list);
                }
                catch (err) { };
            },
            function (err) {
                console.error(err);

            }
        );
    };

}]);
/* OutOfLight Services */
EDPDistribuicaoOutOfLight.factory('OutOfLightService', ['$q', '$timeout', function ($q, $timeout) {

    var getWizzard = function () {

        return {
            $promise: $q.when({
                "Success": true,
                "Result": {
                    "Matrix": {
                        "questions": [
                            {
                                "id": 0,
                                "isStart": true,
                                "header": "Assistente de avarias",
                                "question": "Bem vindo ao assistente de avarias da EDP Distribuição. Este assistente permitirá despistar algumas situações mais comuns para a falta de luz em sua casa.",
                                "answers": [
                                    { "id": 1, "answer": "Iniciar", "gotoquestion": 1 }
                                ]
                            },
                            {
                                "id": -1,
                                "isEnd": true,
                                "header": "Problema solucionado",
                                "question": "Viva a nossa energia. Obrigado",
                                "isSolved": true,
                                "answers": [
                                    { "id": 1, "answer": "Fechar assistente", "gotoexternalaction": "MENU" }
                                ]
                            },
                            {
                                "id": -2,
                                "isEnd": true,
                                "header": "Estou sem luz",
                                "question": "<b>Pode ser uma avaria na rede.</b><br/>Ligue para registarmos o incidente (chamada grátis).",
                                "issueType": "AVR02",
                                "isSolved": false,
                                "answers": [
                                    { "id": 1, "answer": "Contactar assistente", "gotoexternalaction": "CALL_AGENT" }
                                ]
                            },
                            {
                                "id": -3,
                                "isEnd": true,
                                "header": "Estou sem luz",
                                "question": "<b>Pode ser uma avaria na rede.</b><br/>Ligue para registarmos o incidente (chamada grátis).",
                                "issueType": "AVR03",
                                "isSolved": false,
                                "answers": [
                                    { "id": 1, "answer": "Contactar assistente", "gotoexternalaction": "CALL_AGENT" }
                                ]
                            },
                            {
                                "id": -4,
                                "isEnd": true,
                                "header": "Estou sem luz",
                                "question": "<b>Pode ser uma avaria na rede.</b><br/>Ligue para registarmos o incidente (chamada grátis).",
                                "issueType": "AVR01",
                                "isSolved": false,
                                "answers": [
                                    { "id": 1, "answer": "Contactar assistente", "gotoexternalaction": "CALL_AGENT" }
                                ]
                            },
                            {
                                "id": -98,
                                "isEnd": true,
                                "header": "Estou sem luz",
                                "question": "<b>Problema técnico.</b><br/>Trata-se de uma avaria na sua instalação elétrica, a qual deverá ser reparada por um técnico devidamente habilitado.",
                                "isSolved": false,
                                "answers": [
                                    { "id": 1, "answer": "Fechar assistente", "gotoexternalaction": "MENU" }
                                ]
                            },
                            {
                                "id": -99,
                                "isEnd": true,
                                "header": "Estou sem luz",
                                "question": "<b>Pode ser uma avaria na rede.</b><br/>Ligue para registarmos o incidente (chamada grátis).",
                                "isSolved": false,
                                "answers": [
                                    { "id": 1, "answer": "Contactar assistente", "gotoexternalaction": "CALL_AGENT" }
                                ]
                            },
                            {
                                "id": -100,
                                "question": "Selecione o local de consumo referente à avaria",
                                "isChooseCPE": true,
                                "answers": [
                                    { "id": 1, "answer": "Ok", "gotodatastep": "endstep" }
                                ]
                            },
                            {
                                "id": 1,
                                "question": "Ficou sem energia quando ligou algum aparelho?",
                                "answers": [
                                    { "id": 1, "answer": "Não", "gotoquestion": 2 },
                                    { "id": 2, "answer": "Sim", "gotoquestion": 100 }
                                ]
                            },
                            {
                                "id": 2,
                                "question": "Os seus vizinhos têm luz?",
                                "answers": [
                                    { "id": 1, "answer": "Não", "gotoquestion": -100, "endstep": -4 },
                                    { "id": 2, "answer": "Sim", "gotoquestion": 5 }
                                ]
                            },
                            {
                                "id": 5,
                                "question": "Por favor, vá ao quadro geral da sua instalação elétrica.",
                                "answers": [
                                    { "id": 1, "answer": "Ok", "gotoquestion": 6 }
                                ]
                            },
                            {
                                "id": 6,
                                "question": "O disjuntor geral, ou algum dos disjuntores do quadro geral estão desligados?",
                                "answers": [
                                    { "id": 3, "answer": "Ajuda", "gotoaction": "H_SB", "header": "Ajuda quadro geral" },
                                    { "id": 1, "answer": "Não", "gotoquestion": 7 },
                                    { "id": 2, "answer": "Sim", "gotoquestion": 8 }
                                ]
                            },
                            {
                                "id": 7,
                                "question": "Então desligue, por favor, todos os disjuntores, incluindo o disjuntor geral.",
                                "answers": [
                                    { "id": 1, "answer": "Ok", "gotoquestion": 9 }
                                ]
                            },
                            {
                                "id": 8,
                                "question": "Ligue os disjuntores que estiverem desligados.",
                                "answers": [
                                    { "id": 1, "answer": "Ok", "gotoquestion": 26 }
                                ]
                            },
                            {
                                "id": 9,
                                "question": "Agora ligue o disjuntor geral.",
                                "answers": [
                                    { "id": 1, "answer": "Ok", "gotoquestion": 10 }
                                ]
                            },
                            {
                                "id": 10,
                                "question": "E agora ligue os disjuntores do quadro geral uma um.",
                                "answers": [
                                    { "id": 1, "answer": "Ok", "gotoquestion": 11 }
                                ]
                            },
                            {
                                "id": 11,
                                "question": "Todos os disjuntores do quadro geral estão ligados?",
                                "answers": [
                                    { "id": 1, "answer": "Não", "gotoquestion": 12 },
                                    { "id": 2, "answer": "Sim", "gotoquestion": 21 }
                                ]
                            },
                            {
                                "id": 12,
                                "question": "Existe algum disjuntor do quadro geral que não consiga ligar ou que esteja a fazer disparar o disjuntor geral?",
                                "answers": [
                                    { "id": 1, "answer": "Não", "gotoquestion": 10 },
                                    { "id": 2, "answer": "Sim", "gotoquestion": 15 }
                                ]
                            },
                            {
                                "id": 15,
                                "question": "Tem equipamentos de potência elevada ligados simultaneamente?",
                                "answers": [
                                    { "id": 3, "answer": "Ajuda", "gotoaction": "H_HP", "header": "Equipamneto de alta potência" },
                                    { "id": 1, "answer": "Não", "gotoquestion": -100, "endstep": -98 },
                                    { "id": 2, "answer": "Sim", "gotoquestion": 17 }
                                ]
                            },
                            {
                                "id": 17,
                                "question": "Desligue esses equipamentos de potência elevada.",
                                "answers": [
                                    { "id": 1, "answer": "Ok", "gotoquestion": 18 }
                                ]
                            },
                            {
                                "id": 18,
                                "question": "E agora ligue os disjuntores do quadro geral um a um.",
                                "answers": [
                                    { "id": 1, "answer": "Ok", "gotoquestion": 19 }
                                ]
                            },
                            {
                                "id": 19,
                                "question": "Todos os disjuntores do quadro geral estão ligados?",
                                "answers": [
                                    { "id": 1, "answer": "Não", "gotoquestion": 20 },
                                    { "id": 2, "answer": "Sim", "gotoquestion": 21 }
                                ]
                            },
                            {
                                "id": 20,
                                "question": "Existe algum disjuntor do quadro geral que não consiga ligar ou que esteja a fazer disparar o disjuntor geral?",
                                "answers": [
                                    { "id": 1, "answer": "Não", "gotoquestion": 18 },
                                    { "id": 2, "answer": "Sim", "gotoquestion": 15 }
                                ]
                            },
                            {
                                "id": 21,
                                "question": "A energia foi totalmente restabelecida?",
                                "answers": [
                                    { "id": 1, "answer": "Não", "gotoquestion": 22 },
                                    { "id": 2, "answer": "Sim", "gotoquestion": -1 }
                                ]
                            },
                            {
                                "id": 22,
                                "question": "A energia foi restabelecida apenas parcialmente?",
                                "answers": [
                                    { "id": 1, "answer": "Não", "gotoquestion": -100, "endstep": -3 },
                                    { "id": 2, "answer": "Sim", "gotoquestion": -100, "endstep": -2 }
                                ]
                            },
                            {
                                "id": 26,
                                "question": "A energia foi restabelecida?",
                                "answers": [
                                    { "id": 1, "answer": "Não", "gotoquestion": 27 },
                                    { "id": 2, "answer": "Sim", "gotoquestion": -1 }
                                ]
                            },
                            {
                                "id": 27,
                                "question": "Então desligue, por favor, todos os disjuntores, incluindo o disjuntor geral.",
                                "answers": [
                                    { "id": 1, "answer": "Ok", "gotoquestion": 29 }
                                ]
                            },
                            {
                                "id": 29,
                                "question": "Agora ligue o disjuntor geral",
                                "answers": [
                                    { "id": 1, "answer": "Ok", "gotoquestion": 30 }
                                ]
                            },
                            {
                                "id": 30,
                                "question": "E agora ligue os disjuntores do quadro geral um a um.",
                                "answers": [
                                    { "id": 1, "answer": "Ok", "gotoquestion": 31 }
                                ]
                            },
                            {
                                "id": 31,
                                "question": "Todos os disjuntores do quadro geral estão ligados?",
                                "answers": [
                                    { "id": 1, "answer": "Não", "gotoquestion": 32 },
                                    { "id": 2, "answer": "Sim", "gotoquestion": 21 }
                                ]
                            },
                            {
                                "id": 32,
                                "question": "Existe algum disjuntor do quadro geral que não consiga ligar ou que esteja a fazer disparar o disjuntor geral?",
                                "answers": [
                                    { "id": 1, "answer": "Não", "gotoquestion": 30 },
                                    { "id": 2, "answer": "Sim", "gotoquestion": -100, "endstep": -98 }
                                ]
                            },
                            {
                                "id": 100,
                                "question": "Desligue , por favor, o referido aparelho da corrente elétrica.",
                                "answers": [
                                    { "id": 1, "answer": "Ok", "gotoquestion": 101 }
                                ]
                            },
                            {
                                "id": 101,
                                "question": "Por favor, vá ao quadro geral da sua instalação elétrica.",
                                "answers": [
                                    { "id": 1, "answer": "Ok", "gotoquestion": 102 }
                                ]
                            },
                            {
                                "id": 102,
                                "question": "O disjuntor geral, ou algum dos disjuntores do quadro geral estão desligados?",
                                "answers": [
                                    { "id": 3, "answer": "Ajuda", "gotoaction": "H_SB", "header": "Ajuda quadro geral" },
                                    { "id": 1, "answer": "Não", "gotoquestion": 103 },
                                    { "id": 2, "answer": "Sim", "gotoquestion": 104 }
                                ]
                            },
                            {
                                "id": 103,
                                "question": "Então desligue, por favor, todos os disjuntores, incluindo o disjuntor geral.",
                                "answers": [
                                    { "id": 1, "answer": "Ok", "gotoquestion": 105 }
                                ]
                            },
                            {
                                "id": 104,
                                "question": "Ligue os disjuntores que estiverem desligados.",
                                "answers": [
                                    { "id": 1, "answer": "Ok", "gotoquestion": 26 }
                                ]
                            },
                            {
                                "id": 105,
                                "question": "Agora ligue o disjuntor geral.",
                                "answers": [
                                    { "id": 1, "answer": "Ok", "gotoquestion": 106 }
                                ]
                            },
                            {
                                "id": 106,
                                "question": "E agora ligue os disjuntores do quadro geral um a um.",
                                "answers": [
                                    { "id": 1, "answer": "Ok", "gotoquestion": 107 }
                                ]
                            },
                            {
                                "id": 107,
                                "question": "Todos os disjuntores do quadro geral estão ligados?",
                                "answers": [
                                    { "id": 1, "answer": "Não", "gotoquestion": 12 },
                                    { "id": 2, "answer": "Sim", "gotoquestion": 21 }
                                ]
                            }
                        ]
                    }
                }, "JSONMessage": " - No errors"
            })
        };

    }

    return {
        GetWizzard: getWizzard
    }
}]);

